/** 
 * AR Electronics 
 *  
 * @HorribleGeek
 *  
 * HorribleGeek@disroot.org
 *  
╔═══╦═══╗╔═══╦╗░░░░░░╔╗░░░░░░░░░░░░░░░░ 
║╔═╗║╔═╗║║╔══╣║░░░░░╔╝╚╗░░░░░░░░░░░░░░░ 
║║░║║╚═╝║║╚══╣║╔══╦═╩╗╔╬═╦══╦═╗╔╦══╦══╗ 
║╚═╝║╔╗╔╝║╔══╣║║║═╣╔═╣║║╔╣╔╗║╔╗╬╣╔═╣══╣ 
║╔═╗║║║╚╗║╚══╣╚╣║═╣╚═╣╚╣║║╚╝║║║║║╚═╬══║ 
╚╝░╚╩╝╚═╝╚═══╩═╩══╩══╩═╩╝╚══╩╝╚╩╩══╩══╝ 
 
     * You may think you know what the following code does. 
     * But you dont. Trust me. 
     * Fiddle with it, and you'll spend many a sleepless 
     * night cursing the moment you thought you'd be clever 
     * enough to "optimize" the code below. 
     * Now close this file and go play with something else. 
     * As When I wrote this, only God and I understood what I was doing 
     * Now, God only knows
     */  
import java.util.*; 
public class AR_Electronics 
{ 
     public void logo() 
       { 
         System.out.print('\f'); 
         System.out.println("------------------------------------------------------------------------------------"); 
         System.out.println(" |      ┌───┬───┐┌───┬┐░░░░░░░┌┐░░░░░░░░░░░░░░░░      |"); 
         System.out.println(" |      │┌─┐│┌─┐││┌──┤│░░░░░░┌┘└┐░░░░░░░░░░░░░░░      |"); 
         System.out.println(" |      ││░││└─┘││└──┤│┌──┬──┼┐┌┼─┬──┬─┐┌┬──┬──┐      |"); 
         System.out.println(" |      │└─┘│┌┐┌┘│┌──┤│││─┤┌─┘│││┌┤┌┐│┌┐┼┤┌─┤──┤      |"); 
         System.out.println(" |      │┌─┐│││└┐│└──┤└┤│─┤└─┐│└┤││└┘│││││└─┼──│      |"); 
         System.out.println(" |      └┘░└┴┘└─┘└───┴─┴──┴──┘└─┴┘└──┴┘└┴┴──┴──┘      |"); 
         System.out.println("------------------------------------------------------------------------------------"); 
         System.out.println(); 
       } 
      public void main(double Amt, int Qty) 
    { 
        String Slt="Select one from the given list by entering the Number beside it! Enter 0 to Start Again!."; 
        int Qty1=Qty,Var,CP; 
        AR_Electronics ob=new AR_Electronics(); 
        Scanner in= new Scanner(System.in); 
        ob.logo(); 
        System.out.println(" -----------------"); 
        System.out.println(" | Main Category |"); 
        System.out.println(" -----------------"); 
        System.out.println("Select one from the given list by entering the Number beside it!"); 
        System.out.println("1. Camera & Video"); 
        System.out.println("2. Cell Phones"); 
        System.out.println("3. PC & Accessories"); 
        System.out.println("4. Sound"); 
        //System.out.println("5. TV & Accessories"); 
        System.out.println(); 
        System.out.print("Enter your Choice: "); 
        int Main_Category_Select=in.nextInt(); 
        if(Main_Category_Select==1) 
        { 
            ob.logo(); 
            System.out.println(" ------------------"); 
            System.out.println(" | Camera & Video |"); 
            System.out.println(" ------------------"); 
            System.out.println(Slt); 
            System.out.println("1. DSLRs"); 
            System.out.println("2. Mirrorless Cameras"); 
            System.out.println("3. Security Cameras"); 
            System.out.println("4. Camcorders"); 
            System.out.println(); 
            System.out.print("Enter your Choice: "); 
            int Camera_Video_Select=in.nextInt(); 
            switch(Camera_Video_Select) 
              { 
                  case 0: 
                  ob.main(0,0); 
                  break; 
                  case 1: 
                  ob.logo(); 
                  System.out.println(" ---------"); 
                  System.out.println(" | DSLRs |"); 
                  System.out.println(" ---------");  
                  System.out.println(Slt); 
                  System.out.println("1. Nikon D7500              -       1,10,000 INR"); 
                  System.out.println("2. Canon EOS 60D            -       90,000   INR"); 
                  System.out.println("3. Nikon D3400              -       40,000   INR"); 
                  System.out.println("4. Canon Digital Rebel XTi  -       25,000   INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int DSLRs_Select=in.nextInt(); 
                  switch(DSLRs_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=110000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=90000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=40000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=25000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 2: 
                  ob.logo(); 
                  System.out.println(" ----------------------"); 
                  System.out.println(" | Mirrorless Cameras |"); 
                  System.out.println(" ----------------------");  
                  System.out.println(Slt); 
                  System.out.println("1. Sony a7R                 -       1,80,000 INR"); 
                  System.out.println("2. Sony Alpha a6500         -       95,000   INR"); 
                  System.out.println("3. Fujifilm X-E1            -       32,000   INR"); 
                  System.out.println("4. Olympus E-PL7            -       30,000   INR"); 
                  System.out.println("5. YI Gimbal                -       15,000   INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int MirrorlessC_Select=in.nextInt(); 
                  switch(MirrorlessC_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=180000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=95000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=32000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=30000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 5: 
                      CP=15000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 3: 
                  ob.logo(); 
                  System.out.println(" --------------------"); 
                  System.out.println(" | Security Cameras |"); 
                  System.out.println(" --------------------");  
                  System.out.println(Slt); 
                  System.out.println("1. Blink XT                 -       20,000 INR"); 
                  System.out.println("2. Amcrest IP3M             -       7,800   INR"); 
                  System.out.println("3. KeeKoon XP               -       6,500   INR"); 
                  System.out.println("4. Victure Trail            -       5,000   INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int SecurityC_Select=in.nextInt(); 
                  switch(SecurityC_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=20000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=7800; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=6500; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=5000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 4: 
                  ob.logo(); 
                  System.out.println(" --------------"); 
                  System.out.println(" | Camcorders |"); 
                  System.out.println(" --------------");  
                  System.out.println(Slt); 
                  System.out.println("1. Canon XA30               -       1,00,000 INR"); 
                  System.out.println("2. Sony FDRAX53             -       65,000   INR"); 
                  System.out.println("3. Canon VIXIA              -       20,000   INR"); 
                  System.out.println("4. Sony HDRCX405            -       16,000   INR"); 
                  System.out.println("5. KINGEAR HDV-301M         -       7,000    INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int CCorders_Select=in.nextInt(); 
                  switch(CCorders_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=100000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=65000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=20000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=16000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 5: 
                      CP=7000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  default: 
                  error(Amt,Qty); 
                    } 
                } 
           else if(Main_Category_Select==2) 
           { 
             ob.logo(); 
             System.out.println(" ---------------"); 
             System.out.println(" | Cell Phones |"); 
             System.out.println(" ---------------"); 
             System.out.println(Slt); 
             System.out.println("1. Carrier Phones"); 
             System.out.println("2. Unlocked Phones"); 
             System.out.println(); 
             System.out.print("Enter your Choice: "); 
             int CellP_Select=in.nextInt(); 
             switch(CellP_Select) 
              { 
                  case 0: 
                  ob.main(0,0); 
                  break; 
                  case 1: 
                  ob.logo(); 
                  System.out.println(" ------------------"); 
                  System.out.println(" | Carrier Phones |"); 
                  System.out.println(" ------------------");  
                  System.out.println(Slt); 
                  System.out.println("1. iPhone 6 32GB AT&T       -       20,000 INR"); 
                  System.out.println("2. Galaxy S5 16BGB AT&T     -       13,000 INR"); 
                  System.out.println("3. LG Leon T-Mobile         -       4,000  INR"); 
                  System.out.println("4. LG Xpression Verizon     -       3,000  INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int CarrierP_Select=in.nextInt(); 
                  switch(CarrierP_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=20000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=13000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=4000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=3000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 2: 
                  ob.logo(); 
                  System.out.println(" -------------------"); 
                  System.out.println(" | Unlocked Phones |"); 
                  System.out.println(" -------------------");  
                  System.out.println(Slt); 
                  System.out.println("1. Huawei Mate 9 64GB       -       30,000 INR"); 
                  System.out.println("2. Moto X 32GB              -       26,000 INR"); 
                  System.out.println("3. Galaxy J7 Prime 32GB     -       14,000 INR"); 
                  System.out.println("4. Moto G- 4th Gen 32GB     -       13,000 INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int UnlockedP_Select=in.nextInt(); 
                  switch(UnlockedP_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=30000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=26000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=14000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=13000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  default: 
                  error(Amt,Qty); 
            } 
         } 
         else if(Main_Category_Select==3) 
           { 
             ob.logo(); 
             System.out.println(" -------------------"); 
             System.out.println(" | PC & Accessories |"); 
             System.out.println(" -------------------"); 
             System.out.println(Slt); 
             System.out.println("1. Desktops"); 
             System.out.println("2. Laptops"); 
             System.out.println(); 
             System.out.print("Enter your Choice: "); 
             int PC_Accessories_Select=in.nextInt(); 
             switch(PC_Accessories_Select) 
              { 
                  case 0: 
                  ob.main(0,0); 
                  break; 
                  case 1: 
                  ob.logo(); 
                  System.out.println(" ------------"); 
                  System.out.println(" | Desktops |"); 
                  System.out.println(" ------------");  
                  System.out.println(Slt); 
                  System.out.println("1. CYBERPOWERPC VR GXiVR8080A   -       1,20,000 INR"); 
                  System.out.println("2. HP ENVY                      -         99,000 INR"); 
                  System.out.println("3. iBUYPOWER AM900Z             -         85,000 INR"); 
                  System.out.println("4. HP Pavilion                  -         50,000 INR"); 
                  System.out.println("5. Dell Optiplex                -         16,000 INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int Desktops_Select=in.nextInt(); 
                  switch(Desktops_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=120000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=99000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=85000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=50000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 5: 
                      CP=16000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 2: 
                  ob.logo(); 
                  System.out.println(" -----------"); 
                  System.out.println(" | Laptops |"); 
                  System.out.println(" -----------");  
                  System.out.println(Slt); 
                  System.out.println("1. MSI GT73VR                  -       1,65,000 INR"); 
                  System.out.println("2. Apple MacBook Pro           -       1,30,000 INR"); 
                  System.out.println("3. ASUS ROG Strix GL702VS      -       1,00,000 INR"); 
                  System.out.println("4. Acer Predator Helios 300    -         70,000 INR"); 
                  System.out.println("5. Lenovo Flex 5               -         65,000 INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int Laptops_Select=in.nextInt(); 
                  switch(Laptops_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=165000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=130000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=100000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=70000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 5: 
                      CP=65000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  default: 
                  error(Amt,Qty); 
            } 
         } 
         else if(Main_Category_Select==4) 
         { 
             ob.logo(); 
             System.out.println(" ---------"); 
             System.out.println(" | Sound |"); 
             System.out.println(" ---------"); 
             System.out.println(Slt); 
             System.out.println("1. Headphones"); 
             System.out.println("2. Bluetooth Speakers"); 
             System.out.println(); 
             System.out.print("Enter your Choice: "); 
             int Sound_Select=in.nextInt(); 
             switch(Sound_Select) 
              { 
                  case 0: 
                  ob.main(0,0); 
                  break; 
                  case 1: 
                  ob.logo(); 
                  System.out.println(" --------------"); 
                  System.out.println(" | Headphones |"); 
                  System.out.println(" --------------");  
                  System.out.println(Slt); 
                  System.out.println("1. Bose QuietComfort 35         -      23,000 INR"); 
                  System.out.println("2. Sony WH1000XM2               -      20,000 INR"); 
                  System.out.println("3. Sony XB950B1                 -      18,000 INR"); 
                  System.out.println("4. Beats Solo3                  -      16,000 INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int Headphones_Select=in.nextInt(); 
                  switch(Headphones_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=23000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=20000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=18000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 4: 
                      CP=16000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  case 2: 
                  ob.logo(); 
                  System.out.println(" ----------------------"); 
                  System.out.println(" | Bluetooth Speakers |"); 
                  System.out.println(" ----------------------");  
                  System.out.println(Slt); 
                  System.out.println("1. JBL Flip 2               -       9,000 INR"); 
                  System.out.println("2. Sony DreamSound          -       7,000 INR"); 
                  System.out.println("3. Lenevo BT Speaker        -       5,000 INR"); 
                  System.out.println(); 
                  System.out.print("Enter your Choice: "); 
                  int BT_Speakers_Select=in.nextInt(); 
                  switch(BT_Speakers_Select) 
                  { 
                      case 0: 
                      ob.main(0,0); 
                      break; 
                      case 1: 
                      CP=9000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 2: 
                      CP=7000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      case 3: 
                      CP=5000; 
                      PChoice(Amt,Qty,Qty1,CP); 
                      break; 
                      default: 
                      error(Amt,Qty); 
                    } 
                  break; 
                  default: 
                  error(Amt,Qty); 
             } 
            } 
            else 
            { 
                error(Amt,Qty); 
             } 
        } 
    public void error(double Amt,int Qty) 
       { 
          Scanner in= new Scanner(System.in); 
          System.out.print('\f'); 
          System.out.println("Hmm... Error... Please Try Again!"); 
          System.out.println("Enter 0 to Exit or 1 to Start Again!"); 
          int Error=in.nextInt(); 
          if(Error==0) 
          { 
              System.exit(0); 
            } 
          else if(Error==1) 
          { 
          System.out.print('\f'); 
          System.out.println("The Product(s) you have added to your Cart is preserved.  (If Any)"); 
          System.out.println("Redirecting you to Main Category... Please Wait..."); 
          for(double x=-999999;x<999999999;x++); 
          main(Amt,Qty); 
        } 
        else 
        { 
            System.out.print('\f'); 
            System.out.println("Just Leave..."); 
            for(double x=-999999;x<999999999;x++); 
            System.exit(0); 
        } 
       } 
    public void PChoice(double Amt,int Qty,int Qty1,int CP) 
       { 
         int Var; 
         AR_Electronics ob=new AR_Electronics(); 
         Scanner in= new Scanner(System.in); 
         System.out.print("Enter Quantity: "); 
         Qty1=in.nextInt(); 
         Amt=Amt+(CP*Qty1); 
         Qty=Qty+Qty1; 
         ob.logo(); 
         System.out.println("The Product(s) has been added to your Cart. Enter 1 to Continue Shopping or 0 to Print Bill."); 
         Var=in.nextInt(); 
         Switch(Amt,Qty,Var); 
       } 
    public void Switch(double Amt,int Qty, int Var) 
      { 
         switch(Var) 
          { 
             case 0: 
             Bill(Amt,Qty); 
             break; 
             case 1: 
             main(Amt,Qty); 
             break; 
             default: 
             error(Amt,Qty); 
          } 
      } 
    public static void Bill(double Amt,int Qty) 
       { 
         AR_Electronics ob=new AR_Electronics(); 
         ob.logo(); 
         System.out.println("There are "+Qty+" Item(s) in your Cart."); 
         System.out.println("Total Amount : "+Amt); 
         double Tax=Amt+(Amt*18/100); 
         System.out.println("Tax to be Paid(18%): "+(Tax-Amt)); 
         System.out.println("Total Amount to be Paid : "+Tax); 
         System.out.println(); 
         if(Tax>10000 && Tax<100000) 
         { 
             System.out.println("Hurray!! Extra 2% Discount, Only for You!"); 
             double FinalAmt=Tax-(Tax*2/100); 
             System.out.println(); 
             System.out.println("Final Amount: "+FinalAmt); 
            } 
         else if(Tax>100000) 
         { 
             System.out.println("Hurray!! Extra 5% Discount, Only for You!"); 
             double FinalAmt=Tax-(Tax*5/100); 
             System.out.println(); 
             System.out.println("Final Amount: "+FinalAmt); 
            } 
         System.out.println("----------------------------------"); 
         System.out.println("Thank You for visiting AR Electronics!"); 
       } 
    public static void main(String args[]) 
      { 
        AR_Electronics ob=new AR_Electronics(); 
        ob.main(0,0); 
      } 
}